FROM continuumio/conda-ci-linux-64-python3.9
USER root

WORKDIR /app

# Create the environment
COPY ctool.yml ctool.yml
RUN conda env create -f ctool.yml
RUN apt-get update -y && apt-get install -y --no-install-recommends build-essential gcc libsndfile1 libasound2 libfreetype6

# Make RUN commands use the new env
SHELL ["conda", "run", "-n", "ctool", "/bin/bash", "-c"]

# Starting the main code
COPY ./ ./
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "ctool", "python", "autoapp.py"]
