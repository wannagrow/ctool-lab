import warnings
from pathlib import Path
from typing import Tuple, Union

import librosa
import numpy as np
import numpy.typing as npt
import seaborn as sns
import soundfile as sf
import matplotlib.pyplot as plt
from scipy.signal import stft

from editor.utils import load_config
from editor.reverb_estimation.signal_processing import normalize_rms, apply_effects

sns.set()
warnings.filterwarnings("ignore")  # supress librosa warning when reading mp3


def spectrum(path: Path, plot: bool = True, normalize: bool = False) -> Tuple:
    """
    Spectral audio representation in dB/frequency scale
    Args:
        path (Path): path to audiofile
        plot (bool): if need a visualization
        normalize (bool): normalization flag

    Returns:
        Tuple: magnitude values, frequency values,
               sample rate and stft parameters applied

    """
    stft_params = load_config()["stft"]
    audio, fs = librosa.load(path)

    # convert to mono if more than 1 channel
    if len(audio.shape) > 1:
        audio = audio[0, :]

    # resampling to 16 kHz if needed
    if fs != 16000:
        audio = librosa.resample(audio, fs, 16000)
        fs = 16000

    # normalization is needed for correct spectres comparing
    if normalize:
        audio = normalize_rms(audio)

    spec = np.abs(stft(audio, fs, **stft_params)[-1])
    mag = 10 * np.log10(np.array(np.mean(spec, axis=1).flatten()))  # convert to dB
    # convert to Hz scale
    freq = np.arange(1, len(mag) + 1) * fq_resolution(fs, stft_params["nfft"])

    # visualize
    if plot:
        plt.figure(figsize=(15, 8))
        plt.plot(freq, mag)
        plt.xlabel("Frequency (Hz)", size=16)
        plt.ylabel("Level (dB)", size=16)
        plt.show()

    return mag, freq, fs, stft_params["nfft"]


def compare_specs(path1: Path, path2: Path, get_points: bool = True,
                  plot: bool = True) -> Union[Tuple, None]:
    """
    Spectral comparison of two audios with getting frequency points of
    the most different parts.
    Args:
        path1 (Path): path to the first audio
        path2 (Path): path to the second audio
        get_points (bool): if diff points are needed
        plot (bool): if visualization of comparison needed

    Returns:
        Tuple: frequency points and difference values
    """
    if not (get_points or plot):
        print(
            "You should choose if you want to get "
            "correction points or/and a visualization!"
        )

    spec1, freq, fs, nfft = spectrum(path1, plot=False, normalize=True)
    spec2 = spectrum(path2, plot=False, normalize=True)[0]

    # finding frequencies with more than 6 dB difference for equalizer correction
    points, diffs = None, None
    if get_points:
        points, diffs = correction_points(spec1, spec2, fs, nfft)

    if plot:
        plt.figure(figsize=(15, 8))
        plt.plot(freq, spec1, label=path1.stem)
        plt.plot(freq, spec2, c="mediumseagreen", label=path2.stem)
        if points is not None:
            plt.scatter(
                freq[points],
                ((spec1 + spec2) / 2)[points],
                marker="X",
                c="k",
                s=200,
                zorder=10,
            )
        plt.fill_between(freq, spec1, spec2, where=spec1 > spec2, color="r", alpha=0.3)
        plt.fill_between(freq, spec1, spec2, where=spec1 < spec2, color="b", alpha=0.3)
        plt.legend(prop={"size": 16})
        plt.xlabel("Frequency (Hz)", size=16)
        plt.ylabel("Level (dB)", size=16)
        plt.show()

    if points is not None:
        points = np.array(points) * fq_resolution(fs, nfft)

    return points.astype(np.int32), diffs


def fq_resolution(fs: int, nfft: int) -> float:
    """
    Getting the frequency resolution with given samples rate and nfft values.
    Args:
        fs (int): sample rate
        nfft (int): nfft value

    Returns:
        float: frequency resolution
    """
    res = (fs / 2) / (1 + nfft / 2)
    return res


def correction_points(spec1: npt.NDArray, spec2: npt.NDArray, fs: int, nfft: int,
                      min_diff_db: int = 6, lowcut: int = 100, low_fq_gap: int = 150,
                      high_fq_gap: int = 500, fq_threshold: int = 2000) -> Tuple:
    """
    Getting the points of where input spectrums differ more than by reference values
    Args:
        spec1 (npt.NDArray): spectrum of the first audio
        spec2 (npt.NDArray): spectrum of the second audio
        fs (int): sample rate
        nfft (int): nfft size
        lowcut (int): lower frequency bound of finding correction points
        min_diff_db (int): the minimum magnitude difference to be a valid point
        low_fq_gap (int): the minimum width of the band of difference to be a valid
                          point (applies to frequencis below 2kHz)
        high_fq_gap (int): the minimum width of the band of difference to be a valid
                           point (applies to frequencis above 2kHz)
        fq_threshold (int): frequency separating "low" and "high" frequencies in terms
                            of checking valid points

    Returns:
        Tuple: frequency points and difference values
    """
    resolution = fq_resolution(fs, nfft)

    # point is valid if specs diff is more than 6 dB and the interval
    # of one spec being above the other is more than stated *
    # * more than `low_fq_gap` at frequencies below 2 kHz (`high_fq_gap` if above 2 kHz)
    def valid(_diff, _start, _end, _fq_threshold, low_cut):
        if np.abs(_diff[_start:_end]).max() < min_diff_db \
           or _start < low_cut / resolution:
            # print('False. Diff < 6 dB')
            return False
        sub = _end - _start
        if _end < int(_fq_threshold / resolution) \
           and sub > int(low_fq_gap / resolution):
            ok = True
        elif sub > high_fq_gap / resolution:
            ok = True
        else:
            ok = False
            # print('False. Too short frequency interval.')
        return ok

    diff = spec1 - spec2
    signs = np.sign(diff)
    start = 0
    points = []
    diffs = []
    for i in range(len(signs) - 1):
        # diff changing it's sign means we have a new point candidate
        if signs[i] != signs[i + 1] or i == len(signs) - 2:
            end = i + 1
            if not valid(diff, start, i + 1, fq_threshold, lowcut):
                start = i + 1
                continue
            point = (end + start) // 2
            points.append(point)
            diffs.append(diff[start:end].mean())
            start = i + 1

    return points, diffs


def correct_spectrum(path: Union[Path, str], points: npt.NDArray, diffs: npt.NDArray,
                     out_path: Union[Path, str]) -> npt.NDArray:
    """
    Spectral correction using Equalizer plugin
    Args:
        path (Union): path to an audio to be corrected
        points (npt.NDArray): list of frequencies
        diffs (npt.NDArray): list of difference values (dB)
        out_path (Union): path to write the result

    Returns:
        npt.NDArray: corrected audio
    """
    effect_params = load_config()["effects"]

    # for now performing only one correction
    idx = np.argmax(diffs)
    effect_params["equalizer"]["mid_freq_hz"] = points[idx]
    effect_params["equalizer"]["mid_gain_db"] = diffs[idx]

    audio, fs = sf.read(path)
    if fs != 16000:
        audio = librosa.resample(audio, fs, 16000)
        fs = 16000
    audio = normalize_rms(audio)

    audio = apply_effects(
        audio, effect_params, fs, noisegate=False, reverb=False, compressor=False
    )

    sf.write(out_path, audio, fs)

    return audio
