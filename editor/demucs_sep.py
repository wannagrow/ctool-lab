"""
Using Facebook's Demucs for music source separation
"""

import subprocess as sp
from pathlib import Path
from typing import Tuple, List, Union

from editor import load_config


def find_files(in_path: Union[str, Path], extensions: Tuple = ("wav", "mp3")) -> List:
    """
    Searching for valid audios in a folder.

    Args:
        in_path (str): path to a folder
        extensions (Tuple): tuple of valid file extensions

    Returns:
        List: list of valid files' paths
    """
    out = []
    for file in Path(in_path).iterdir():
        if file.suffix.lower().lstrip(".") in extensions:
            out.append(file)
    return out


def separate(inp: Union[str, Path], outp: Union[str, Path],
             one_file: bool = True, verbose: bool = False):
    """
    Perform source separation with demucs.

    Args:
        inp (str): path to a folder or a file to stem
        outp (str): path to an output folder
        one_file (bool): if input was a path to a file
        verbose (bool): printing info
    """
    demucs_params = load_config()["demucs"]

    cmd = [
        "demucs",
        "-o", str(outp),
        "-n", "mdx_extra",
        "-d", demucs_params["device"],
        "--shifts", str(demucs_params["shifts"]),
        "--overlap", str(demucs_params["overlap"]),
        "--two-stems", "vocals"
    ]
    if demucs_params["no_split"]:
        cmd.append("--no-split")

    if demucs_params["mp3"]:
        cmd += ["--mp3", f"--mp3-bitrate={demucs_params['mp3_rate']}"]

    if one_file:
        assert Path(inp).suffix.lower() in [
            ".wav",
            ".mp3",
        ], 'Unknown file extension! Should be either ".wav" or ".mp3"!'
        files = [inp]
    else:
        files = [str(f) for f in find_files(inp)]
        if not files:
            print(f"No valid audio files in {inp}")
            return

    if verbose:
        print("Going to separate the files:")
        print(files)
        print("With command: ", " ".join(cmd))
        print()

    p = sp.Popen(cmd + files, shell=False, stderr=sp.PIPE, stdout=sp.PIPE)
    p.communicate()
