from pathlib import Path
from typing import Dict

import ruamel.yaml as yaml


def get_root() -> Path:
    return Path(__file__).parent.parent


def load_config() -> Dict:
    """
    Loading parameters' values from a yaml
    """
    root = get_root()
    with open(root / "config.yaml") as f:
        args = yaml.load(f, Loader=yaml.Loader)

    return args
