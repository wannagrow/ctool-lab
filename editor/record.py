import numpy as np
import numpy.typing as npt
import librosa
import warnings
import soundfile as sf
from pathlib import Path
from pedalboard import Pedalboard, NoiseGate

from editor.utils import get_root
from editor import (load_config, separate, apply_effects, get_rms,
                    normalize_rms, mix, map_reverbs, compare_specs)

warnings.filterwarnings("ignore")


class AudioEdit:
    """
    Audio processor. Controls audio stemming, mastering and mixing.
    """

    def __init__(self, song_path: str):
        """
        Setting up for a cover. Initializing paths and variables,
        getting a song's stems with demucs, then noise gating vocals
        to get the cleaner instrumental.

        Args:
            song_path (str): path to a song
        """
        self.cfg_effects = load_config()["effects"]
        self.original, self.fs = librosa.load(song_path, sr=None, mono=False)
        self.original = self.original.transpose()
        self.instrumental = np.array([])  # instrumental got after self._subtract_vocals
        self.vocals = np.array([])  # acapella extracted from a song with demucs
        self.acapella = np.array([])  # recorded user acapella (getting from stream)
        self.effected = np.array([])  # mastered acapella
        self.result = np.array([])  # final result

        root = get_root()
        self.demucs_path = root / "cover_tool_app/static/users_data/result_demucs"
        self.path = self.demucs_path / f"mdx_extra/{Path(song_path).stem}"
        self.vocals_path = self.path / "vocals.wav"
        self.instrumental_path = self.path / "instrumental.wav"
        self.acapella_path = self.path / "acapella.wav"
        self.result_path = self.path / "result.wav"

        if self.vocals_path.exists():  # song has been stemmed earlier so vocals exist
            self.vocals, _ = librosa.load(str(self.vocals_path), sr=None, mono=False)
            self.vocals = self.vocals.transpose()
        else:
            separate(song_path, self.demucs_path)
            self.vocals, _ = librosa.load(str(self.vocals_path), sr=None, mono=False)
            self.vocals = self.vocals.transpose()

        gated = self._noise_gating(self.vocals)
        self._subtract_vocals(gated)

    def _noise_gating(self, audio: npt.NDArray, attack_ms: float = 0.02,
                      ratio: float = 1, release_ms: float = 0.1,
                      threshold_db: float = -40.0) -> npt.NDArray:
        """
        Cleaning the input audio from background noise (or tiny artifacts
        after separating with machine learning methods).

        Args:
            audio (np.array): input audio
            attack_ms (float): NoiseGate plugin parameter (triggering time ON)
            ratio (float): NoiseGate plugin parameter (snr?)
            release_ms (float): NoiseGate plugin parameter (triggering time OFF)
            threshold_db (float): NoiseGate plugin parameter (noise level to supress)

        Returns:
            np.array: cleaned audio
        """
        gate = Pedalboard(
            [
                NoiseGate(
                    attack_ms=attack_ms,
                    ratio=ratio,
                    release_ms=release_ms,
                    threshold_db=threshold_db,
                )
            ]
        )
        vocals_gated = gate(audio, sample_rate=self.fs)
        return vocals_gated

    def _subtract_vocals(self, gated: npt.NDArray):
        """
        Subtracts the cleaned vocals from the original song to get instrumental,
        then saves the result.

        Args:
            gated (np.array): input vocals (supposed to be cleaned before)
        """
        self.instrumental = self.original - gated
        sf.write(self.instrumental_path, self.instrumental, self.fs)

    def get_reverb(self) -> float:
        """
        Applies the reverb estimator to get the room size parameter
        needed in the Reverb plugin.

        Returns:
            float: estimated room size parameter value
        """
        df = map_reverbs(self.path, ["acapella", "vocals"], "cpu")
        vocals_reverb = df[df["file"] == "vocals.wav"].values[0, 1]
        return vocals_reverb

    def spectrum_correction(self):
        """
        Comparison of user's and source's vocals to get parameters values
        for Equalizer plugin.
        """
        corr_points, diffs = compare_specs(
            self.vocals_path, self.acapella_path, plot=False
        )
        if len(corr_points) > 0:
            idx = np.argmax(diffs)
            point, diff = corr_points[idx], diffs[idx]
            self.cfg_effects["equalizer"]["mid_freq_hz"] = point
            self.cfg_effects["equalizer"]["mid_gain_db"] = diff

    def apply_effects(self) -> npt.NDArray:
        """
        Applies the effects chain to user's acapella.

        Returns:
            np.array: "effected" user acapella
        """
        audio = self.acapella
        audio = normalize_rms(audio)
        audio = np.vstack([audio.reshape(-1), audio.reshape(-1)]).T
        audio = apply_effects(audio, self.cfg_effects, self.fs)
        self.effected = audio
        sf.write(self.path / "effected.wav", audio, self.fs)
        return audio

    def mastering(self) -> npt.NDArray:
        """
        Estimation of the source's reverberation, estimation of the
        Equalizer parameters, applying the effects chain.

        Returns:
            np.array: mastered user's acapella ready for the final mix
        """
        vocal_reverb = self.get_reverb()
        self.cfg_effects["reverb"]["size"] = vocal_reverb

        self.spectrum_correction()
        audio = self.apply_effects()
        return audio

    def mix(self):
        """
        Mixing of instrumental and effected user acapella,
        saving the result.
        """
        vocals_db = get_rms(self.vocals, db=True)
        self.effected = normalize_rms(self.effected, vocals_db)
        self.result = mix(self.effected, self.instrumental)
        sf.write(self.result_path, self.result, self.fs)
