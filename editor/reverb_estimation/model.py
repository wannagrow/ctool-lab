from torch import nn


class GuessNet(nn.Module):
    def __init__(self, ch_in=1, channels=5):
        super().__init__()

        self.net = nn.Sequential(
            nn.Conv2d(ch_in, channels, (1, 10), (1, 2)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Conv2d(channels, channels, (1, 10), (1, 3)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Conv2d(channels, channels, (1, 11), (1, 3)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Conv2d(channels, channels, (1, 11), (1, 2)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Conv2d(channels, channels, (3, 8), (2, 2)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Conv2d(channels, channels, (4, 7), (2, 1)),
            nn.ReLU(),
            nn.BatchNorm2d(channels),
            nn.Dropout(0.5),
            nn.Flatten(start_dim=1),
            nn.Linear(60 * channels, 1)
        )

    def forward(self, x):
        return self.net(x)
