import numpy as np
import pandas as pd
from typing import Tuple, List

import torch
from torch.utils.data import DataLoader
from pathlib import Path
from editor.reverb_estimation.model import GuessNet
from editor.reverb_estimation.dataset import GuessTest
from editor.utils import get_root


def inference(model: GuessNet, test_loader: DataLoader, device: str = "cuda") -> Tuple:
    """
    Applies the model to the test data.
    Args:
        model (GuessNet): pytorch model
        test_loader (DataLoader): DataLoader object
        device (str): device to perform inference on

    Returns:
        Tuple: prediction values, file paths
    """
    model.to(device)
    model.eval()
    predictions = []
    files = []
    for i, (sample, sample_path) in enumerate(test_loader):
        sample = sample.to(device)
        with torch.no_grad():
            pred = model(sample).squeeze().cpu()
            predictions.append(pred)
            files.append(sample_path)

    predictions = np.concatenate(predictions)
    files = [item for sublist in files for item in sublist]
    return predictions, files


def map_reverbs(folder: Path, exact_names: List = None, device="cuda") -> pd.DataFrame:
    """
    Loading the pretraind model and applying it to the files in test folder.
    Args:
        folder (Path): path to the test folder
        exact_names (List): list of filenames to process
        device (str): device to perform inference on

    Returns:
        pd.DataFrame: dataframe with predictions
    """
    root = get_root()
    state = torch.load(
        root / "editor/pretrained/init_best_score.pth",
        map_location=torch.device(device),
    )
    model = GuessNet()
    model.load_state_dict(state["model"])

    test = GuessTest(folder, exact_names)
    loader = DataLoader(
        test,
        batch_size=min(256, len(test)),
        shuffle=False,
        num_workers=2,
        pin_memory=True,
        drop_last=False,
    )

    preds, files = inference(model, loader, device)

    df = pd.DataFrame(
        zip([Path(x).name for x in files], preds),
        columns=["file", "predicted_room_size"],
    )

    df["file"] = df.file.apply(lambda x: "".join(x.split("_")[1:]))
    df = df.groupby("file", as_index=False).mean().sort_values(by="file")
    df["predicted_room_size"] = df.predicted_room_size.astype(int)

    return df
