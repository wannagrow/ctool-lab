import os
from typing import List
from pathlib import Path

import torch
from torch.utils.data import Dataset

from editor.reverb_estimation.gtgram.plot import render_audio_from_file
from editor.reverb_estimation.signal_processing import (normalize, crop,
                                                        spectral_whitening)


class GuessTest(Dataset):
    def __init__(self, folder: Path, exact_names: List = None):
        crop(folder, 4, exact_names)  # splitting to 4 sec samples
        new_folder = folder / 'cropped'
        self.filenames = [new_folder / file for file in os.listdir(new_folder)]

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        file_path = str(self.filenames[idx])
        spectrum = render_audio_from_file(file_path)[:, :-1]
        spectrum = spectral_whitening(spectrum)
        spectrum = normalize(spectrum)
        spectrum = torch.Tensor(spectrum).float()

        return spectrum.unsqueeze(0), file_path
