import os
import shutil
from pathlib import Path
from typing import List, Dict

import librosa
import numpy as np
import numpy.typing as npt
import soundfile as sf
import torch
from torchaudio.functional import equalizer_biquad
from pedalboard import Pedalboard, NoiseGate, Compressor, load_plugin

from editor.utils import get_root


def apply_effects(audio: npt.NDArray, params: Dict, fs: int = 44100,
                  noisegate: bool = True, reverb: bool = True,
                  compressor: bool = True, equalizer: bool = True) -> npt.NDArray:
    """
    Applying a chain of effects to an input

    Note: Equalizer plugin effect was replaced with torch.functional.equalizer_biquad
          due to OS issues (TDR_SlickEQ vst3 plugin is available only for Windows and
          Mac users). According to this change the correction is point-wise and having
          more than 1 point only one will be chosen. Correcting more points may be
          added in later versions.

    Args:
        audio (np.array): input audio file
        params (Dict): effects parameters values
        fs (int): sampling frequency
        noisegate (bool): if including Noise Gate effect
        reverb (bool): if including Reverberation effect
        compressor (bool): if including Compressor effect
        equalizer (bool): if including Equalizer effect

    Returns:
        np.array: processed audio
    """
    root = get_root()
    reverb_plugin = load_plugin(
        str(root / "editor/data/vst3_plugins/TAL-Reverb-4.vst3")
    )

    if reverb:
        for attr in params["reverb"].keys():
            setattr(reverb_plugin, attr, params["reverb"][attr])

    board = Pedalboard([])
    if equalizer:
        audio = equalizer_biquad(torch.Tensor(audio), fs,
                                 params["equalizer"]["mid_freq_hz"],
                                 params["equalizer"]["mid_gain_db"]).numpy()
    if noisegate:
        board.append(NoiseGate(**params["noise_gate"]))
    if compressor:
        board.append(Compressor(**params["compressor"]))
    if reverb:
        board.append(reverb_plugin)
    audio = board(audio, sample_rate=fs)
    return audio


def mix(acapella: npt.NDArray, instrumental: npt.NDArray,
        amp: float = 1.0) -> npt.NDArray:
    """
    Mixing acapella with instrumental.

    Args:
        acapella (np.array): input acapella audio
        instrumental (np.array): input instrumental audio
        amp (float): instrumental to vocals ratio

    Returns:
        np.array: mix of the inputs
    """
    multiplier = abs(instrumental).max() / abs(acapella).max() * amp
    return (instrumental + acapella * multiplier) / (1 + amp)


def get_rms(x: npt.NDArray, db: bool = False) -> float:
    """
    Estimates the RMS level of an audio.

    Args:
        x (np.array): input audio
        db (bool): return the result in dB instead of RMS flag

    Returns:
        float: the RMS or dB level of an input
    """
    level = np.sqrt((x**2).mean())
    if db:
        level = to_db(level)
    return level


def to_db(level: float) -> float:
    """
    Converts RMS level to dB.

    Args:
        level (float):

    Returns:
        float: dB level
    """
    return 10 * np.log10(level)


def normalize_rms(x: npt.NDArray, level_db: float = -10) -> npt.NDArray:
    """
    Audio RMS-normalization.

    Args:
        x (np.array): input audio
        level_db (float): normalization level (dB)

    Returns:
        np.array: normalized audio
    """
    level = 10 ** (level_db / 10)
    coef = np.sqrt((len(x) * level**2) / np.sum(x**2))
    return coef * x


def normalize(arr: npt.NDArray) -> npt.NDArray:
    """
    Standard normalization.

    Args:
        arr (np.array): input array

    Returns:
        np.array: normalized array
    """
    return (arr - arr.mean(axis=1, keepdims=True)) / arr.std(axis=1, keepdims=True)


def spectral_whitening(spectrum: npt.NDArray) -> npt.NDArray:
    """
    Spectral whitening.

    Args:
        spectrum (np.array): input spectrum array

    Returns:
        np.array: whitened spectrum array
    """
    median = np.median(spectrum, axis=-1, keepdims=True)
    return spectrum - median


def crop(folder: Path, duration: int, exact_names: List = None):
    """
    Cutting acapellas and instrumentals to the same length samples

    Args:
        folder (str): path to a folder with audios
        duration (int): lengths in seconds
        exact_names (List): list of filenames you want to process
    """
    files = [file for file in os.listdir(folder) if os.path.isfile(folder / file)]
    if exact_names:
        files = [file for file in files if file[:-4] in exact_names]
    out_path = folder / "cropped"
    if os.path.exists(out_path):
        shutil.rmtree(out_path)

    os.mkdir(out_path)

    for file in files:
        audio, fs = librosa.load(folder / file)
        if fs != 16000:
            audio = librosa.resample(audio, fs, 16000)
            fs = 16000

        if len(audio.shape) > 1:
            audio = audio.T[:, 0]

        audio_rms = get_rms(audio, True)

        # TODO
        # insert voice activity detection

        k = 0
        duration_frames = duration * fs
        for i in range(0, len(audio) - duration_frames, duration_frames):
            sample = audio[i : i + duration_frames]
            if len(sample) < duration_frames:
                sample = np.hstack([sample, np.zeros(duration_frames - len(sample))])
            if audio_rms - get_rms(sample, True) > 5:
                continue
            sf.write(out_path / (f"{k}_" + file[:-4] + ".wav"), sample, fs)
            k += 1
