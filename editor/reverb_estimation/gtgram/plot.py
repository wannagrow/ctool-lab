# Copyright 2014 Jason Heeris, jason.heeris@gmail.com
#
# This file is part of the gammatone toolkit, and is licensed under the 3-clause
# BSD license: https://github.com/detly/gammatone/blob/master/COPYING
"""
Plotting utilities related to gammatone analysis, primarily for use with
``matplotlib``.
"""
import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np
import scipy.constants
import scipy.io.wavfile

from editor.reverb_estimation.gtgram.filters import erb_point
from editor.reverb_estimation.gtgram.fftweight import fft_gtgram


plt.rcParams["figure.figsize"] = 15, 8

DEFAULT_DURATION = 4


class ERBFormatter(matplotlib.ticker.EngFormatter):
    """
    Axis formatter for gammatone filterbank analysis. This formatter calculates
    the ERB spaced frequencies used for analysis, and renders them similarly to
    the engineering axis formatter.
    The scale is changed so that `[0, 1]` corresponds to ERB spaced frequencies
    from ``high_freq`` to ``low_freq`` (note the reversal). It should be used
    with ``imshow`` where the ``extent`` argument is ``[a, b, 1, 0]`` (again,
    note the inversion).
    """

    def __init__(self, low_freq, high_freq, *args, **kwargs):
        """
        Creates a new :class ERBFormatter: for use with ``matplotlib`` plots.
        Note that this class does not supply the ``units`` or ``places``
        arguments; typically these would be ``'Hz'`` and ``0``.
        :param low_freq: the low end of the gammatone filterbank frequency range
        :param high_freq: the high end of the gammatone filterbank frequency
          range
        """
        self.low_freq = low_freq
        self.high_freq = high_freq
        super().__init__(*args, **kwargs)

    def _erb_axis_scale(self, fraction):
        return erb_point(self.low_freq, self.high_freq, fraction)

    def __call__(self, val, pos=None):
        newval = self._erb_axis_scale(val)
        return super().__call__(newval, pos)


def gtgram_plot(gtgram_function, axes, x, fs, window_time,
                hop_time, channels, f_min, imshow_args=None):
    """
    Plots a spectrogram-like time frequency magnitude array based on gammatone
    subband filters.
    :param gtgram_function: A function with signature::
        fft_gtgram(
            wave,
            fs,
            window_time, hop_time,
            channels,
            f_min)
    See :func:`gammatone.gtgram.gtgram` for details of the paramters.
    """
    # Set a nice formatter for the y-axis
    # formatter = ERBFormatter(f_min, 6000, unit='Hz', places=0)
    # axes.yaxis.set_major_formatter(formatter)

    # Figure out time axis scaling
    # duration = len(x) / fs

    # Calculate 1:1 aspect ratio
    # aspect_ratio = duration/scipy.constants.golden

    gtg = gtgram_function(x, fs, window_time, hop_time, channels, f_min)
    # Z = np.flipud(20 * np.log10(gtg))

    z = 20 * np.log10(np.where(gtg == 0, 1e-6, gtg))

    # img = axes.imshow(Z, extent=[0, duration, 1, 0], aspect=aspect_ratio)
    # axes.imshow(Z, extent=[0, duration, 1, 0], cmap='magma')
    # plt.gca().set_aspect('auto')

    return z


# Entry point for CLI script

HELP_TEXT = """\
Plots the gammatone filterbank analysis of a WAV file.
If the file contains more than one channel, all channels are averaged before
performing analysis.
"""


def render_audio_from_file(path, duration=DEFAULT_DURATION, function=fft_gtgram):
    """
    Renders the given ``duration`` of audio from the audio file at ``path``
    using the gammatone spectrogram function ``function``.
    """
    samplerate, data = scipy.io.wavfile.read(path)

    nframes = duration * samplerate
    signal = data[:nframes]

    # Default gammatone-based spectrogram parameters
    twin = 0.004
    thop = twin / 2
    channels = 21
    fmin = 400

    # Set up the plot
    # fig = plt.figure()
    # axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    axes = None

    z = gtgram_plot(function, axes, signal, samplerate, twin, thop, channels, fmin)

    # axes.set_title(os.path.basename(path))
    # axes.set_xlabel("Time (s)")
    # axes.set_ylabel("Frequency")

    # plt.show()

    return z


if __name__ == "__main__":
    """
    Entry point for CLI application to plot gammatonegrams of sound files.
    """

    # file = '../data/set/1/10/rev_14_2_0.wav'
    # duration = 4
    # function = gammatone.fftweight.fft_gtgram
    #
    # spec = render_audio_from_file(file, duration, function)
