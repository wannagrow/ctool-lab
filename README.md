# Cover tool

In this repository there is a tool that allows recording a cover on a song.  
Having an audiofile it's possible to create a custom version of it without
using any other programs.  
Passing a recording of original song as input a user receives its instrumental 
version and asked to record his own acapella. The output is a mix of 
processed user vocal recording and song's instrumental.  

### Usage

**Creating the environment**

Using anaconda you can create the environment with the following command:
```
 conda env create -f ctool.yml
```

Originally project was using vst3 plugins both for reverberation and equalization
which are mostly compatible only with Windows / Mac. For making it possible to run this
project on Linux, equalization was replaced with `torchaudio.functional.equalizer.biquad` 
and reverb with it's Linux version. It has some extra dependencies that can be installed with:
```
  apt-get update -y && apt-get install -y --no-install-recommends build-essential gcc libsndfile1 libasound2 libfreetype6
```

**Running the project**  
You can start CoverTool on localhost with `set FLASK_APP=autoapp.py` and then `flask run`  
Or just `python autoapp.py`

You can also use docker running `docker build -t covertool .` from the root directory 
and then start a container with `docker run -p 5000:5000 covertool`  


**Quick tutorial**  
1. After running the app you will be offered to upload an mp3/wav file with a song.
Press Submit button and wait until the song is stemmed into vocals and instrumental, it can take 
up to several minutes depending on the audio length and cpu / gpu properties.  
2. If it's not your first time covering this particular song (on your local machine) 
the stems can be just loaded, so you don't have to perform this operation twice. 
After recording there is an option to record again if the result is not satisfying.  
3. The next step is calibration. Due to different latencies of input and output devices there 
can be a mismatch of acapella and instrumental timing. Move the slider and listen if the 
parts of audio match. When you hear no latency just press the Ready button.  
4. Now everything is almost ready. Wait until the mastering process is over and get your cover!  

### Pipeline

**Stemming**  
Getting vocals and accompaniment out of a song is performed by [demucs](https://github.com/facebookresearch/demucs) 
out-of-the-box. Then the vocals is processed with noise gate to get rid of artifacts 
and the gated version is subtracted from the original track in order to get
the instrumental.  

**Acapella processing**  
The processing of raw recording is divided into two steps: estimating effects
parameters and applying effects. For reverberation [TAL-Reverb-4](https://tal-software.com/products/tal-reverb-4)
 is used with all parameters frozen except the `room_size`.
This parameter is estimated using the network architecture proposed in 
[this article](https://www.microsoft.com/en-us/research/uploads/prod/2018/09/Blind_reverberation_time_estimation_Gamper_IWAENC_2018.pdf).
The second effect that requires some estimation is Equalizer (this version is using [TDR VOS SlickEQ](https://www.tokyodawn.net/tdr-vos-slickeq/)).
Comparing spectrum of original vocals and user acapella the point of correction and
the correction strength are found. Then knowing all the necessary parameters 
the following chain of effects is applied (all the effects are applied using 
[pedalboard](https://github.com/spotify/pedalboard)):  
Equalizer -> Noise Gate -> Compressor -> Reverb

**Mixing**  
The last step is mixing of processed acapella and original instrumental. All we need to handle here is
the level ratio of these tracks. Current solution is using the estimated level of original acapella.  


## Estimation of Reverberation effect

One of the goals of CoverTool is to reproduce acoustic conditions of the vocals in a source song.
Raw recordings are often dry and need to be processed properly. One of the key effects
in acapellas mastering is reverberation effect. In this pipeline [TAL-Reverb-4](https://tal-software.com/products/tal-reverb-4)
audio plugin is used, freezing all the parameters except the `room_size` that correlates with reverberation time.  
The estimation of this parameter was performed using the architecture and data preprocessing
described in [this article](https://www.microsoft.com/en-us/research/uploads/prod/2018/09/Blind_reverberation_time_estimation_Gamper_IWAENC_2018.pdf).
The proposed method was used for "real" reverberation time estimation while we worked with artificial 
reverberation that doesn't use the "reverberation time" term as parameter, it depends on several factors.
It was supposed that knowing `room_size` is enough to reproduce original reverberation with minor audial
differences indistinguishable after mixing with instrumental.  
  
### Data
The dataset was formed using free audio books read by 43 different speakers. All the recordings were reverberated
using different `room_size` parameter values. All the audios were RMS-normalized and divided into 4 seconds samples
with 3 seconds overlap. Low energy samples (more than 20 dB lower than RMS of all the whole recording)
were dropped. .  

After processing through a [gammatone ERB filterbank](https://github.com/detly/gammatone), 
spectral whitening and normalization we get the resulting feature matrix:  
  
![img.png](assets/feature_matrix.png)

  
### Architecture
Block diagram of CNN architecture:  
  
![img.png](assets/architecture.png)


## Equalizer parameters setting

In this part of the project user acapella is analysed and compared to the original
vocals of the song. The spectral analysis can help us find a set of points 
where user signal can be slightly corrected in order to sound cleaner and closer
to the original.
To apply this information using Equalizer plugin / function it's also needed to know 
"the strength" of this correction. The comparison may look like this:  

![img.png](assets/spectrum_comparison.png)

The marks are set in the points where the difference between two lines is more
than the reference value (can be tuned) and this pit band is large enough.
After finding the correction points and strengths, these values can be passed
to the plugin.  


### Further steps  
1. Equalizer correction may be done in several points
2. Reverb estimation can be enhanced by collecting more data and increasing model complexity
3. Reverb can be also estimated and applied separately on different vocal parts of audio -> become more flexible
4. Mastering can be enhanced by using more processing tools and modifying the effects chain