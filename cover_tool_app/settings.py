import os
from dotenv import load_dotenv

basedir = os.path.abspath("..")
load_dotenv(os.path.join(basedir, "../.env"))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY")
