from flask import Flask
from cover_tool_app.settings import Config
from cover_tool_app import public
from cover_tool_app.extensions import bootstrap, moment


def create_app():
    """Create application factory"""
    app = Flask(__name__)
    app.config.from_object(Config)
    register_extensions(app)
    register_blueprints(app)
    # register_errorhandlers(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    moment.init_app(app)
    bootstrap.init_app(app)
    return


def register_blueprints(app):
    app.register_blueprint(public.views.blueprint)
    return


# def register_errorhandlers(app):
#     """Register error handlers."""
#
#     def render_error(error):
#         """Render error template."""
#         # If a HTTPException, pull the `code` attribute; default to 500
#         error_code = getattr(error, "code", 500)
#         return render_template(f"{error_code}.html"), error_code
#
#     for errcode in [401, 404, 500]:
#         app.errorhandler(errcode)(render_error)
#     return
