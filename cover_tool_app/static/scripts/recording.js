var path = document.currentScript.getAttribute("acapella_path");
console.log("path: ", path);
var name = document.currentScript.getAttribute("name");
console.log("name: ", name);

var startButton = document.getElementById("start-record");
var audio = document.getElementById("instrumental");
var AudioPlay = document.getElementById("play-record");
var readyButton = document.getElementById("ready");
var recordingLabel = document.getElementById("recording-label");


startButton.addEventListener("click", recordUser);
readyButton.addEventListener("click", redirect);


function recordUser(){
    AudioPlay.controls = false;
    readyButton.style.visibility = "hidden";
    console.log("ready-button hidden and disabled");

    navigator.mediaDevices.getUserMedia({audio:true, video:false}).then(function(stream){
        const chunks = [];
        const mediaRecorder = new MediaRecorder(stream, {mimeType:'audio/webm'});

        mediaRecorder.addEventListener('dataavailable', function (e) {
            if (e.data.size > 0) chunks.push(e.data);
        });
        console.log("created mediaRecorder");

        mediaRecorder.addEventListener("stop", function(e){
            console.log("recorder stopped");

            AudioPlay.controls = true;
            const audioBlob = new Blob(chunks, {'type' : 'audio/webm'})
            AudioPlay.src = URL.createObjectURL(audioBlob);
            console.log("recorded audio displayed");

            startButton.disabled = false;
            recordingLabel.style.display = "none";
            readyButton.style.visibility = "visible";
            readyButton.disabled = false;
            console.log("ready-button enabled and displayed");

            sendAudioFile(new File([audioBlob], "acapella.wav"));
            console.log("file is saved");
        });

        audio.addEventListener("ended", function(){
            console.log("instrumental has finished playing");
            mediaRecorder.stop();
            stream.getTracks().forEach(track => track.stop());
        });

        startButton.disabled = true;
        recordingLabel.style.display = "block";
        mediaRecorder.start();
        console.log('started recording');
        audio.play();
        console.log("playing instrumental");

});
}


const sendAudioFile = (file) => {
  const formData = new FormData();
  formData.append('audio-file', file);
  return fetch("../save_recording/" + path, {
    method: 'POST',
    body: formData
  });
};


function redirect(){
    window.location.href = "../calibration/" + name;
};
