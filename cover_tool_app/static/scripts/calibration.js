var elem = document.querySelector('input[type="range"]');
var instrumental = document.getElementById("instrumental");
var acapella = document.getElementById("acapella");
var readyButton = document.getElementById("ready");
var name = document.currentScript.getAttribute("name");
console.log("song name: ", name);

var rangeValue = function() {
    var newValue = elem.value;
    var target = document.querySelector('.value');
    target.innerHTML = newValue + " ms";
    console.log("slider value changed");
}

elem.addEventListener("input", rangeValue);
elem.addEventListener("change", playNewVersion);
readyButton.addEventListener("click", redirect);


function playNewVersion() {
    instrumental.pause();
    instrumental.currentTime = 0;
    console.log("instrumental stopped playing, currentTime set to ", instrumental.currentTime);
    acapella.pause();
    acapella.currentTime = elem.value / 1000;
    console.log("acapella stopped playing, currentTime set to ", acapella.currentTime);
    instrumental.play();
    acapella.play();
    console.log("instrumental and acapella are playing simultaneously");
}


function redirect(){
    window.location.href = "../mastering/" + name + '/' + elem.value;
};
