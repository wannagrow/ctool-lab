var revChecker = document.getElementById("rev-check");
var specChecker = document.getElementById("spec-check");
var effChecker = document.getElementById("effects-check");
var mixDiv = document.getElementById("mix");
var resultPath = document.currentScript.getAttribute("result_path");
var name = document.currentScript.getAttribute("name");
var audioResult = document.getElementById("result");


window.onload = reverbEstimation(revChecker)

function reverbEstimation(destElem) {
    console.log("requesting reverb estimation");
    $.post('/estimate_reverb/' + name).done(function(response) {
        console.log("got reverb estimation response: ", response)
        $(destElem).text("Acoustic environment modeling..." + response);
        spectrumCorrection(specChecker);
    }).fail(function() {
        console.log("reverb estimation was failed");
        $(destElem).text('Failed.');
    });
};


function spectrumCorrection(destElem) {
    console.log("requesting spectrum correction");
    $.post('/spectrum_correction/' + name).done(function(response) {
        console.log("got spectrum correction response: ", response)
        $(destElem).text("Spectrum correction..." + response);
        applyEffects(effChecker);
    }).fail(function() {
        console.log("spectrum correction was failed");
        $(destElem).text('Failed.');
    })
}


function applyEffects(destElem) {
    console.log("requesting effects applying");
    $.post('/apply_effects/' + name).done(function(response) {
        console.log("got effects applying response: ", response);
        $(destElem).text("Applying effects..." + response);
        mixDiv.style.display = "block";
        mixResult();
    }).fail(function() {
        console.log("applying effects was failed");
        $(destElem).text('Failed.');
    });
};


function mixResult() {
    console.log("requesting new mix");
    audioResult.controls = false;
    $.post('/mixing/' + name).done(function(response) {
        console.log("got the mix")
        audioResult.src = "../../" + resultPath;
        audioResult.controls = true;
    });
};
