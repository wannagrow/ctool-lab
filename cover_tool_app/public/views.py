import pickle
import librosa
import numpy as np
from flask import Blueprint, jsonify, flash, redirect, render_template, request, url_for
from werkzeug.utils import secure_filename

from editor.record import AudioEdit

blueprint = Blueprint("public", __name__, static_folder="../static")
is_busy = False


def check_busy():
    return is_busy


def allowed_file(filename: str) -> bool:
    """
    Checks a song's extension is .mp3 or .wav

    Args:
        filename (str): name of a file with extension

    Returns:
        bool: True if file extension is allowed, False otherwise
    """
    valid = "." in filename and filename.split(".")[-1].lower() in ["mp3", "wav"]
    return valid


def save_editor(editor: AudioEdit, name: str):
    """
    Saves an editor instance into pickle.

    Args:
        editor (AudioEdit): instance of AudioEdit class
        name (str): project name
    """
    with open(f"cover_tool_app/static/users_data/{name}.pkl", "wb") as file:
        pickle.dump(editor, file, protocol=pickle.HIGHEST_PROTOCOL)


def load_editor(name: str) -> AudioEdit:
    """
    Loads an editor instance from pickle.

    Args:
        name (str): project name
    Returns:
        AudioEdit: instance of AudioEdit class
    """
    with open(f"cover_tool_app/static/users_data/{name}.pkl", "rb") as file:
        editor = pickle.load(file)
    return editor


@blueprint.route("/")
def index():
    """Home page"""
    return render_template("public/upload.html")


@blueprint.route("/", methods=["GET", "POST"])
def upload_page():
    """
    Initialization of the editor with the uploaded song file
    and saving them.
    """
    audio = request.files["chooseFile"]
    if check_busy():
        flash("The server is busy, please, try again in several minutes.")
        return redirect(url_for("public.index"))
    filename = secure_filename(audio.filename)
    if audio.filename == "":
        flash("There is no file selected")
        return redirect(request.url)
    if audio and allowed_file(filename):
        global is_busy
        is_busy = True
        input_path = "cover_tool_app/static/users_data/" + filename
        audio.save(input_path)
        editor = AudioEdit(input_path)
        save_editor(editor, filename[:-4])
        is_busy = False
        return redirect(url_for("public.recording", name=filename))
    return redirect(url_for("public.index"))


@blueprint.route("/recording/<name>", methods=["GET", "POST"])
def recording(name):
    """Recording page. Performs recording of user's acapella"""
    instrumental_path = (
        f"../static/users_data/" f"result_demucs/mdx_extra/{name[:-4]}/instrumental.wav"
    )
    acapella_path = f"{name[:-4]}"
    return render_template(
        "public/recording.html",
        name=name,
        instrumental_path=instrumental_path,
        acapella_path=acapella_path,
    )


@blueprint.route("/save_recording/<path>", methods=["POST"])
def save_recording(path):
    """Saving user's recording to server"""
    audio = request.files["audio-file"]
    filename = secure_filename(audio.filename)
    input_path = (
        "cover_tool_app/static/users_data/"
        "result_demucs/mdx_extra/" + path + "/" + filename
    )
    audio.save(input_path)
    return jsonify(success=True)


@blueprint.route("/calibration/<name>")
def calibration(name: str):
    """
    Calibration page.
    Helps user to get the latency between acapella and instrumental.

    Args:
        name (str): name of the song (needed to load the files from server)
    """
    instrumental_path = (
        f"../static/users_data/" f"result_demucs/mdx_extra/{name[:-4]}/instrumental.wav"
    )
    acapella_path = (
        f"../static/users_data/" f"result_demucs/mdx_extra/{name[:-4]}/acapella.wav"
    )
    return render_template(
        "public/calibration.html",
        name=name[:-4],
        instrumental_path=instrumental_path,
        acapella_path=acapella_path,
    )


@blueprint.route("/mastering/<name>/<latency_ms>")
def mastering(name, latency_ms):
    """
    Mastering page. Removes the latency, triggers reverb estimation,
    correction of spectrum, applying effects chain and mixing the result.

    Args:
        name (str): name of the song (needed to load the files from server)
        latency_ms (str): latency value (ms)
    """
    editor = load_editor(name)
    acapella_path = (
        f"cover_tool_app/static/users_data/"
        f"result_demucs/mdx_extra/{name}/acapella.wav"
    )
    editor.acapella, a_fs = librosa.load(acapella_path)
    editor.acapella = librosa.resample(editor.acapella, a_fs, editor.fs)

    start = int(editor.fs * (int(latency_ms) / 1000))
    end = int(start + editor.instrumental.shape[0])
    editor.acapella = editor.acapella[start:end]
    diff = editor.instrumental.shape[0] - len(editor.acapella)
    if diff > 0:
        editor.acapella = np.hstack([editor.acapella, np.array([0] * diff)])
    assert len(editor.acapella) == editor.instrumental.shape[0], (
        f"Shapes are not equal: {len(editor.acapella)}(acapella)"
        f" vs {editor.instrumental.shape[0]}(instrumental)"
    )
    save_editor(editor, name)
    result_path = f"static/users_data/result_demucs/" f"mdx_extra/{name}/result.wav"
    return render_template("public/mastering.html", result_path=result_path, name=name)


@blueprint.route("/estimate_reverb/<name>", methods=["POST"])
def estimate_reverb(name):
    """Performs the estimation of room size parameter for Reverb plugin."""
    editor = load_editor(name)
    vocal_reverb = editor.get_reverb()
    editor.cfg_effects["reverb"]["size"] = vocal_reverb
    save_editor(editor, name)
    return jsonify("Done!")


@blueprint.route("/spectrum_correction/<name>", methods=["POST"])
def spectrum_correction(name):
    """Performs the estimation of Equalizer parameters."""
    editor = load_editor(name)
    editor.spectrum_correction()
    save_editor(editor, name)
    return jsonify("Done!")


@blueprint.route("/apply_effects/<name>", methods=["POST"])
def apply_effects(name):
    """Applies the effects chain to acapella."""
    editor = load_editor(name)
    editor.apply_effects()
    save_editor(editor, name)
    return jsonify("Done!")


@blueprint.route("/mixing/<name>", methods=["POST"])
def mixing(name):
    """Mixes the mastered acapella and instrumental."""
    editor = load_editor(name)
    editor.mix()
    return jsonify("Done!")
